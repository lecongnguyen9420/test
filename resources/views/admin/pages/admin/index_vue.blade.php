@extends('admin.share.master_page')
@section('noi_dung')
    <div class="row" id="app">
        <div class="col-md-5">
            <div class="card">
                <form id="taoTaiKhoan" v-on:submit.prevent="themMoiTaiKhoan()">
                    <div class="card-header">
                        Thêm mới tài khoản
                    </div>
                    <div class="card-body">
                        <label>Họ và tên</label>
                        <input name="ho_va_ten" type="text" class="form-control" placeholder="Nhập họ và tên">
                        <label>Email</label>
                        <input name="email" type="text" class="form-control" placeholder="Nhập email">
                        <label>Mật khẩu</label class="form-control">
                        <input name="password" type="password" placeholder="Nhập mật khẩu" class="form-control">
                        <label>NHập lại mật khẩu</label>
                        <input name="re_password" type="password" class="form-control">
                        <label>Số điện thoại</label class="form-control">
                        <input name="so_dien_thoai" type="number" placeholder="Nhập số điện thoại" class="form-control">
                        <label>Ngày sinh</label>
                        <input name="ngay_sinh" type="date" class="form-control" placeholder="Nhập ngày sinh">
                    </div>
                    <div class="card-footer text-end">
                        <button type="submit" class="btn btn-primary">Thêm tài khoản</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-7">
            <div class="card">
                <div class="card-header">
                    Danh Sách Tài Khoản
                </div>
                <div class="card-body" id="table">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Họ Và Tên</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Số Điện Thoại</th>
                                <th class="text-center">Ngày Sinh</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            <template v-for="(v , k ) in listTK">
                                <tr>
                                    <th class="text-center align-middle">@{{ k + 1 }}</th>
                                    <td class="align-middle">@{{ v.ho_va_ten }}</td>
                                    <td class="align-middle">@{{ v.email }}</td>
                                    <td class="align-middle">@{{ v.so_den_thoai }}</td>
                                    <td class="align-middle">@{{ v.ngay_sinh }}</td>
                                    <td class="text-center">
                                        <button class="btn btn-info">Cập Nhật</button>
                                        <button class="btn btn-danger">Xóa Bỏ</button>
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        new Vue({
            el: '#app',

            data: {
                listTK: [],
            },

            created() {
                this.loadData();
            },

            methods: {
                themMoiTaiKhoan() {
                    var paramObj = {};
                    $.each($('#taoTaiKhoan').serializeArray(), function(_, kv) {
                        if (paramObj.hasOwnProperty(kv.name)) {
                            paramObj[kv.name] = $.makeArray(paramObj[kv.name]);
                            paramObj[kv.name].push(kv.value);
                        } else {
                            paramObj[kv.name] = kv.value;
                        }
                    });
                    console.log(paramObj);
                    axios
                        .post('/admin/tai-khoan/create-ajax', paramObj)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success("Đã thêm mới thành công!");
                                this.loadData();
                            }
                        });
                },
                loadData() {
                    axios
                        .get('/admin/tai-khoan/data')
                        .then((res) => {
                            this.listTK = res.data.data;
                        });
                },
            },
        });
    </script>
@endsection
