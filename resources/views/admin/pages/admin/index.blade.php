@extends('admin.share.master_page')
@section('noi_dung')
    <div class="row">
        <div class="col-md-5">
            <div class="card">
                <form id="taoTaiKhoan">
                    <div class="card-header">
                        Thêm mới tài khoản
                    </div>
                    <div class="card-body">
                        <label>Họ và tên</label>
                        <input name="ho_va_ten" type="text" class="form-control" placeholder="Nhập họ và tên">
                        <label>Email</label>
                        <input name="email" type="text" class="form-control" placeholder="Nhập email">
                        <label>Mật khẩu</label class="form-control">
                        <input name="password" type="password" placeholder="Nhập mật khẩu" class="form-control">
                        <label>NHập lại mật khẩu</label>
                        <input name="re_password" type="password" class="form-control">
                        <label>Số điện thoại</label class="form-control">
                        <input name="so_dien_thoai" type="number" placeholder="Nhập số điện thoại" class="form-control">
                        <label>Ngày sinh</label>
                        <input name="ngay_sinh" type="date" class="form-control" placeholder="Nhập ngày sinh">
                    </div>
                    <div class="card-footer text-end">
                        <button type="submit" class="btn btn-primary">Thêm tài khoản</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-7">
            <div class="card">
                <div class="card-header">
                    Danh Sách Tài Khoản
                </div>
                <div class="card-body" >
                    <table class="table table-bordered" id="table_1">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Họ Và Tên</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Số Điện Thoại</th>
                                <th class="text-center">Ngày Sinh</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- @foreach ($data as $key => $value)
                            <tr>
                                <th class="text-center align-middle"> {{$key + 1}}</th>
                                <td class="align-middle">{{$value->ho_va_ten}}</td>
                                <td class="align-middle">{{$value->email}}</td>
                                <td class="align-middle">{{$value->so_dien_thoai}}</td>
                                <td class="align-middle">{{$value->ngay_sinh}}</td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-info">Cập Nhật</button>
                                    <button type="button" class="btn btn-danger">Xóa Bỏ</button>
                                </td>
                            </tr>
                            @endforeach --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    $(document).ready(function(){
        loadData();
        function loadData(){
            $.ajax({
                'url'       :   '/admin/tai-khoan/data',
                'type'      :   'get',
                'success'   :   function(res){
                    var noi_dung = '';
                    $.each(res.data, function(k, v){
                        noi_dung += '<tr>';
                        noi_dung += '<th class="text-center align-middle">'+ (k + 1) + '</th>';
                        noi_dung += '<td class="align-middle">' + v.ho_va_ten + '</td>';
                        noi_dung += '<td class="align-middle">' + v.email + '</td>';
                        noi_dung += '<td class="align-middle">' + v.so_dien_thoai + '</td>';
                        noi_dung += '<td class="align-middle text-nowrap">' + v.ngay_sinh + '</td>';
                        noi_dung += '<td class="text-center text-nowrap">';
                        noi_dung += '<button class="btn btn-info">Cập Nhật</button>';
                        noi_dung += '<button class="btn btn-danger">Xóa Bỏ</button>';
                        noi_dung += '</td>';
                        noi_dung += '</tr>';
                    });
                    $("#table_1 tbody").html(noi_dung);
                }
            });
        };

        $("#taoTaiKhoan").submit(function(e){
            e.preventDefault();
            var paramObj = {};
            $.each($('#taoTaiKhoan').serializeArray(), function(_,kv){
                if(paramObj.hasOwnProperty(kv.name)){
                    paramObj[kv.name] = $.makeArray(paramObj[kv.name]);
                    paramObj[kv.name].push(kv.value);
                }
                else {
                    paramObj[kv.name] = kv.value;
                }
            });

            $.ajax({
                'url'   :   '/admin/tai-khoan/create-ajax',
                'type'  :   'post',
                'data'  :    paramObj,
                'success'   :   function(res){
                    if(res.status){
                        toastr.success("Đã thêm mới thành công!");
                        loadData();
                    }
                }
            });

        });
    });
</script>
@endsection
