@extends('admin.share.master_page')
@section('noi_dung')
    <div class="row mt-3">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">
                    Thêm Mới Chuyên Mục
                </div>
                <div class="card-body">
                    <form action="/admin/chuyen-muc/index" method='POST'>
                        @csrf
                        <label for="inputFirstName" class="form-label">Tên Chuyên Mục</label>
                        <input id="ten_chuyen_muc" type="text" class="form-control" id="inputFirstName">
                        <label for="inputFirstName" class="form-label">Slug Chuyên Mục</label>
                        <input id="slug_chuyen_muc"type="text" class="form-control" id="inputFirstName">
                        <label for="inputFirstName" class="form-label">Tình Trạng</label>
                        <select id="tinh_trang" id="inputState" class="form-select">
                            <option value="1">Hiển Thị</option>
                            <option value="0">Tạm Hết</option>
                        </select>
                        <label for="inputFirstName" class="form-label">Chuyên mục cha</label>
                        <select class="form-control" id="id_chuyen_muc_cha">

                        </select>

                    </form>
                </div>
                <div class="card-footer text-end">
                    <button id="themMoi" type="button" class="btn btn-primary ">Thêm Chuyên Mục</button>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="card">
                <div class="card-header">
                    Danh Sách Chuyên Mục
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="table_chuyen_muc">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Tên Chuyên Mục</th>
                                <th scope="col">Tình Trạng</th>
                                <th scope="col">Chuyên Mục Cha</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        {{-- deleteModal --}}
                        <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Delete Chuyên Mục </h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" class="form-control" id="delete_id"
                                            placeholder="Nhập vào id cần xóa">
                                        <p>Bạn cần chắc chắn xóa Chuyên Mục này. Việc này không thể hoàn tác!</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Đóng</button>
                                        <button id="acceptDelete" type="button" class="btn btn-danger">Đồng ý xóa</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- editModal --}}
                        <div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Cập nhật Chuyên Mục </h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <input class="form-control" type="text" id="update_id">
                                        <label class="form-label">Tên Chuyên Mục</label>
                                        <input id="update_ten_chuyen_muc" type="text" class="form-control">
                                        <label class="form-label">Slug Chuyên Mục</label>
                                        <input id="update_slug_chuyen_muc"type="text" class="form-control">
                                        <label class="form-label">Tình Trạng</label>
                                        <select id="update_tinh_trang" class="form-select">
                                            <option value="1">Hiển Thị</option>
                                            <option value="0">Tạm Hết</option>
                                        </select>
                                        <label class="form-label">Chuyên mục cha</label>
                                        <select class="form-control" id="update_chuyen_muc_cha">
                                            <option value="1">Root</option>


                                        </select>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-bs-dismiss="modal">Đóng</button>
                                        <button id="acceptUpdate" type="button" class="btn btn-primary">Cập
                                            nhật</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            $("#ten_chuyen_muc").keyup(function(){
                var noi_dung    = $("#ten_chuyen_muc").val();
                var slug        = toSlug(noi_dung);
                $("#slug_chuyen_muc").val(slug);
            });
            $("#update_ten_chuyen_muc").keyup(function(){
                var noi_dung    = $("#update_ten_chuyen_muc").val();
                var slug        = toSlug(noi_dung);
                $("#update_slug_chuyen_muc").val(slug);
            });
            function toSlug(str) {
                // Chuyển chuỗi sang chữ thường
                str = str.toLowerCase();

                // Xóa dấu
                str = str.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
                str = str.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
                str = str.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
                str = str.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
                str = str.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
                str = str.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
                str = str.replace(/đ/gi, 'd');

                // Xóa khoảng trắng và ký tự đặc biệt
                str = str.replace(/\s+/g, '-'); // Thay khoảng trắng bằng dấu gạch ngang
                str = str.replace(/[^a-z0-9-]/gi, ''); // Xóa ký tự đặc biệt

                return str;
        }


            //modalUpdate
            $("body").on('click', '#acceptUpdate', function() {
                var id = $("#update_id").val();
                var ten_chuyen_muc = $("#update_ten_chuyen_muc").val();
                var slug_chuyen_muc = $("#update_slug_chuyen_muc").val();
                var tinh_trang = $("#update_tinh_trang").val();
                var id_chuyen_muc_cha = $("#update_chuyen_muc_cha").val();

                var z = {
                    'id': id,
                    'ten_chuyen_muc': ten_chuyen_muc,
                    'slug_chuyen_muc': slug_chuyen_muc,
                    'tinh_trang': tinh_trang,
                    'id_chuyen_muc_cha': id_chuyen_muc_cha
                };

                console.log(z);

                $.ajax({
                    'url': '/admin/chuyen-muc/update',
                    'type': 'post',
                    'data': z,
                    'success': function(res) {
                        if (res.status) {
                            toastr.success("Đã cập nhật chuyên mục thành công!");
                            hienThiTable();
                            $('#editModal').modal('hide');
                        } else {
                            toastr.error("Đã có lỗi xảy ra!");
                        }
                    },
                });

            });
            $("body").on('click', '.sua', function() {
                var id = $(this).data('code');
                // console.log(id);
                // $.ajax({ // cái ni sai, ó sanh dưới nghe
                //     'url'       :       '/admin/chuyen-muc/edit/' + id,
                //     'type'      :       'get';
                //     'success'   :       function{
                //         if(res.status){
                //             // $("#update_id").val(res.data.id);
                //             // $("#update_ten_chuyen_muc").val(res.data.ten_chuyen_muc);
                //             // $("#update_slug_chuyen_muc").val(res.data.slug_chuyen_muc);
                //             // $("#update_tinh_trang").val(res.data.tinh_trang);
                //             // $("#update_chuyen_muc_cha").val(res.data.id_chuyen_muc_cha);

                //         } else {
                //             toastr.error("không được")
                //             $('#editModal').modal('hide');// cái git config á anh

                //         }
                //     },
                // });
                $.ajax({
                    url: "/admin/chuyen-muc/edit/" + id,
                    type: 'get',
                    success: function(res){
                        if(res.status){
                            $("#update_id").val(res.data.id);
                            $("#update_ten_chuyen_muc").val(res.data.ten_chuyen_muc);
                            $("#update_slug_chuyen_muc").val(res.data.slug_chuyen_muc);
                            $("#update_tinh_trang").val(res.data.tinh_trang);
                            $("#update_chuyen_muc_cha").val(res.data.id_chuyen_muc_cha);
                            console.log(res.data);
                        } else {
                            toastr.error("không được")
                            $('#editModal').modal('hide');
                        }
                    }
                });
            });






            //modalDelete
            $("body").on('click', '#acceptDelete', function() {
                var id = $("#delete_id").val();
                $.ajax({
                    'url': '/admin/chuyen-muc/delete/' + id,
                    'type': 'get',
                    'success': function(res) {
                        if (res.status) {
                            //thông báo xóa thành công, hiển thị và tải lại trang
                            toastr.success("Đã xóa Chuyên mục thành công!");
                            hienThiTable();
                            $('#deleteModal').modal('hide');
                        } else {
                            //thông báo lỗi rồi kệ nó
                            toastr.error('Chuyên mục không tồn tại!');
                        }
                    }
                })
            });

            $("body").on('click', '.xoa', function() {
                var id = $(this).data('code');
                $("#delete_id").val(id);
            });

            hienThiTable();


            function hienThiTable() {
                var noi_dung = '';
                var list = [{
                        'id': 1,
                        'ten_chuyen_muc': 'Apple',
                        'tinh_trang': 1,
                        'ten_chuyen_muc_cha': '-'
                    },
                    {
                        'id': 2,
                        'ten_chuyen_muc': 'Iphone',
                        'tinh_trang': 1,
                        'ten_chuyen_muc_cha': 'Apple'
                    },
                    {
                        'id': 3,
                        'ten_chuyen_muc': 'Ipad',
                        'tinh_trang': 0,
                        'ten_chuyen_muc_cha': 'Apple'
                    },
                    {
                        'id': 4,
                        'ten_chuyen_muc': 'Ipod',
                        'tinh_trang': 1,
                        'ten_chuyen_muc_cha': 'Apple'
                    },
                    {
                        'id': 5,
                        'ten_chuyen_muc': 'Áo',
                        'tinh_trang': 1,
                        'ten_chuyen_muc_cha': '-'
                    },
                    {
                        'id': 6,
                        'ten_chuyen_muc': 'Áo Thun',
                        'tinh_trang': 1,
                        'ten_chuyen_muc_cha': 'Áo'
                    },
                    {
                        'id': 7,
                        'ten_chuyen_muc': 'Áo Khoát',
                        'tinh_trang': 1,
                        'ten_chuyen_muc_cha': 'Áo'
                    },
                    {
                        'id': 8,
                        'ten_chuyen_muc': 'Áo Sơ Mi',
                        'tinh_trang': 1,
                        'ten_chuyen_muc_cha': '-'
                    },
                    {
                        'id': 9,
                        'ten_chuyen_muc': 'Quần Tây',
                        'tinh_trang': 1,
                        'ten_chuyen_muc_cha': '-'
                    },
                    {
                        'id': 10,
                        'ten_chuyen_muc': 'Quần Jean',
                        'tinh_trang': 1,
                        'ten_chuyen_muc_cha': '-'
                    },
                    {
                        'id': 11,
                        'ten_chuyen_muc': 'Quần Kaki',
                        'tinh_trang': 1,
                        'ten_chuyen_muc_cha': '-'
                    },
                    {
                        'id': 12,
                        'ten_chuyen_muc': 'Quần Jean Lửng',
                        'tinh_trang': 1,
                        'ten_chuyen_muc_cha': 'Quần Jean'
                    },
                    {
                        'id': 13,
                        'ten_chuyen_muc': 'Quần Jean Rách',
                        'tinh_trang': 1,
                        'ten_chuyen_muc_cha': 'Quần Jean'
                    },
                ];
                $("#themMoi").click(function() {
                    var ten_chuyen_muc = $("#ten_chuyen_muc").val();
                    var slug_chuyen_muc = $("#slug_chuyen_muc").val();
                    var tinh_trang = $("#tinh_trang").val();
                    var id_chuyen_muc_cha = $("#id_chuyen_muc_cha").val();

                    var z = {
                        'ten_chuyen_muc': ten_chuyen_muc,
                        'slug_chuyen_muc': slug_chuyen_muc,
                        'tinh_trang': tinh_trang,
                        'id_chuyen_muc_cha': id_chuyen_muc_cha,
                    };

                    $.ajax({
                        'url': '/admin/chuyen-muc/create',
                        'type': 'post',
                        'data': z,
                        'success': function(res) {
                            if (res.xxx) {
                                toastr.success("Đã thêm mới chuyên mục thành công!");
                                hienThiTable();
                            } else {
                                toastr.error("Thêm mới chuyên mục không thành công!");
                                hienThiTable();
                            }
                        }
                    });

                });
                $.ajax({
                    'url': '/admin/chuyen-muc/data',
                    'type': 'get',
                    'success': function(abc) {
                        var option = '<option value  = "0"> Root </option>';
                        var noi_dung = '';
                        $.each(abc.data, function(k, v) {
                            option += '<option value  = "'+ v.id + '"> '+ v.ten_chuyen_muc + ' </option>';
                            noi_dung += '<tr>';
                            noi_dung += '<th class="align-middle text-center">' + (k + 1) +
                                '</th>';
                            noi_dung += '<td class="align-middle">' + v.ten_chuyen_muc +
                                '</td>';
                            noi_dung += '<td class="align-middle text-center">';
                            if (v.tinh_trang == 1) {
                                noi_dung +=
                                    '<button data-id="' + v.id +
                                    '" class=" doiTrangThai btn btn-success">Hiển Thị</button>';
                            } else {
                                noi_dung +=
                                    '<button data-id="' + v.id +
                                    '" class=" doiTrangThai btn btn-warning">Tạm Tắt</button>';
                            }
                            noi_dung += '</td>';
                            if (v.ten_chuyen_muc_cha == null) {
                                noi_dung += '<td class="align-middle">Root</td>';
                            } else {
                                noi_dung += '<td class="align-middle">' + v.ten_chuyen_muc_cha +
                                    '</td>';
                            }

                            noi_dung += '<td class="text-center">';
                            noi_dung += '<button data-code="' + v.id +
                                '" class="sua btn btn-info" style="magrgin-right 10px" data-bs-toggle="modal" data-bs-target="#editModal">Cập Nhật</button>';
                            noi_dung += '<button data-code="' + v.id +
                                '" class="xoa btn btn-danger " data-bs-toggle="modal" data-bs-target="#deleteModal">Xóa Bỏ</button>';
                            noi_dung += '</td>';
                            noi_dung += '</tr>';
                        });

                        // đưa nội dung lên HtML
                        $("#table_chuyen_muc tbody").html(noi_dung);
                        $("#id_chuyen_muc_cha").html(option);
                        $("#update_chuyen_muc_cha").html(option);
                    }
                })



            }
            $("body").on('click', '.doiTrangThai', function() {
                var id = $(this).data('id');
                $.ajax({
                    'url': '/admin/chuyen-muc/doi-trang-thai/' + id,
                    'type': 'get',
                    'success': function(res) {
                        if (res.status == "True") {
                            toastr.success("Đã đổi trạng thái thành công!");
                            hienThiTable();
                        } else {
                            toastr.error("Dữ liệu không tồn tại");
                            // hienThiTable();
                        }
                    }

                })

            });
        });
    </script>
@endsection
