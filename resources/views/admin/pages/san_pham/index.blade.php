@extends('admin.share.master_page')
@section('noi_dung')
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h6 class="mt-3"><b>Thêm Mới Sản Phẩm</b></h6>
            </div>
            <div class="card-body">
                <label>Tên Sản phẩm</label>
                <input id="ten_san_pham" name="ten_san_pham" type="text" class="form-control">
                <label>Slug Sản phẩm</label>
                <input id="slug_san_pham" name="slug_san_pham" type="text" class="form-control">
                <label>Hình ảnh</label>
                <input id="hinh_anh" name="hinh_anh" type="text" class="form-control">
                <label>Mô Tả</label>
                <textarea class="form-control" name="mo_ta" id="mo_ta" cols="30" rows="3"></textarea>
                {{-- <input id="mo_ta" name="editor1" type="text" class="form-control"> --}}
                <label>Chuyên Mục</label>
                <input id="id_chuyen_muc" name="id_chuyen_muc" type="text" class="form-control">
                <label>Trạng Thái</label>
                <select class="form-control" name="tinh_trang" id="tinh_trang">
                    <option value="1">Hiển Thị</option>
                    <option value="0">Tạm Tắt</option>
                </select>
            </div>
            <div class="card-footer text-end">
                <button type="submit" id="them_moi" class="btn btn-danger">Thêm Mới Sản Phẩm</button>
            </div>
        </div>
    </div>

    <div class="col-md-8">
        <div class="card">
            <table class="table table-bordered" id='table_san_pham'>
                <div class="card-header text-center">
                    <h6 class="mt-2"><b>Danh Sách Sản Phẩm</b></h6>
                </div>
                <thead>
                    <th class="text-center">#</th>
                    <th class="text-center">Tên Sản Phẩm</th>
                    <th class="text-center">Tình Trạng</th>
                    <th class="text-center">Chuyên Mục</th>
                    <th class="text-center">Hình Ảnh</th>
                    <th class="text-center">Mô Tả</th>
                    <th class="text-center">Action</th>
                </thead>
                <tbody>
                    <div class="modal fade" id="capnhat" tabindex="-1" aria-labelledby="cap_nhat_modal"
                    aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header" style="background-color:rgb(246, 175, 124)">
                                <h1 class="modal-title fs-5" id="cap_nhat_modal"><b>Cập Nhật Kích Cở</b></h1>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body" style="background-color:rgb(246, 175, 124)">
                                <input id="update_id" type="hidden" class="form-control">
                                <label>Tên Sản Phẩm</label>
                                <input id="update_ten_san_pham" type="text" class="form-control">
                                <label>Slug Sản Phẩm</label>
                                <input id="update_slug_san_pham" type="text" class="form-control">
                                <label>Hình Ảnh</label>
                                <input id="update_hinh_anh" type="text" class="form-control">
                                <label>Mô Tả</label>
                                <input id="update_mo_ta" type="text" class="form-control">
                                <label>Chuyên Mục</label>
                                <input id="update_id_chuyen_muc" type="text" class="form-control">
                                <label>Tình Trạng</label>
                                <select class="form-control" id="update_tinh_trang">
                                    <option value="1">Đang Kinh Doanh</option>
                                    <option value="0">Ngừng Kinh Doanh</option>
                                </select>
                            </div>
                            <div class="card-footer text-end" style="background-color:rgb(246, 175, 124)">
                                <button style="background-color:rgb(246, 175, 124)" id="btn_submit_cap_nhat"
                                    type="submit" class="btn btn-info">Cập Nhật</button>
                            </div>
                        </div>
                    </div>
                </div>

                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="https://cdn.ckeditor.com/4.20.1/standard/ckeditor.js"></script>
<script>
    $(document).ready(function(){

        hienthi();
        function hienthi(){
            noi_dung = '';
            $.ajax({
                url : '/admin/san-pham/data',
                type : 'get',
                success : function(res){
                $.each(res.data, function(k, v) {
            noi_dung+='<tr>';
            noi_dung+='<td class="text-center">'+(k+1)+'</td>';
            noi_dung+='<th class="text-center">'+v.ten_san_pham+'</th>';
            if(v.tinh_trang==0){
                noi_dung+='<th class="text-center">Ngừng Kinh Doanh</th>';
            } else{
                noi_dung+='<th class="text-center">Đang Kinh Doanh</th>';
            }
            noi_dung+='<th class="text-center">'+v.id_chuyen_muc+'</th>';
            noi_dung+='<th class="text-center">'+v.hinh_anh+'</th>';
            noi_dung+='<th class="text-center">'+v.mo_ta+'</th>';
            noi_dung+='<th class="text-center">';
            noi_dung +=
            '<button type="button" data-id_cap_nhat="' + v.id +
            '" class="btn btn-primary btn_cap_nhat" data-bs-toggle="modal" data-bs-target="#capnhat">Cập Nhật</button>'
            noi_dung+='<a data-id="'+v.id+'" class="btn btn-danger" id="xoabo">Xóa Bỏ</a>';
            noi_dung+='</tr>';
                });
                $("#table_san_pham tbody").html(noi_dung);
                }
            });
        }
        $('body').on('click','.btn_cap_nhat',function(){
            var id = $(this).data('id_cap_nhat');
            $.ajax({
                url : '/admin/san-pham/get-data-id/'+id,
                type : 'get',
                success : function(res){
                    $("#update_id").val(res.data.id),
                    $("#update_ten_san_pham").val(res.data.ten_san_pham),
                    $("#update_slug_san_pham").val(res.data.slug_san_pham),
                    $("#update_tinh_trang").val(res.data.trang_thai),
                    $("#update_mo_ta").val(res.data.mo_ta),
                    $("#update_id_chuyen_muc").val(res.data.id_chuyen_muc),
                    $("#update_hinh_anh").val(res.data.hinh_anh)
                }
            });
        });
        $('body').on('click','#xoabo',function(){
            var id = $(this).data('id');
            $.ajax({
                url : '/admin/san-pham/delete/'+id,
                type : 'get',
                success : function(res){
                    if(res.status){
                        toastr.success('Đã Xóa Id ' + id + ' Thành Công');
                        hienthi();
                    }
                }
            });
        });
        $('body').on('click', '#btn_submit_cap_nhat', function() {
            console.log('a');
            var z = {
                'id' : $("#update_id").val(),
                'ten_san_pham' : $("#update_ten_san_pham").val(),
                'slug_san_pham' : $("#update_slug_san_pham").val(),
                'hinh_anh' : $("#update_hinh_anh").val(),
                'mo_ta' : $("#update_mo_ta").val(),
                'trang_thai' : $("#update_trang_thai").val(),
                'id_chuyen_muc' : $("#update_id_chuyen_muc").val()
            }
            $.ajax({
                url : '/admin/san-pham/update',
                type : 'post',
                data : z,
                success : function(res){
                    if(res.status){
                        toastr.success("Cập Nhật Thành Công");
                        hienthi();
                        $("#capnhat").modal('hide');
                    }
                }
            });
        });
        $("#them_moi").click(function(){
            var z = {
                'ten_san_pham' : $("#ten_san_pham").val(),
                'slug_san_pham' : $("#slug_san_pham").val(),
                'hinh_anh' : $("#hinh_anh").val(),
                'mo_ta' : $("#mo_ta").val(),
                'trang_thai' : $("#trang_thai").val(),
                'id_chuyen_muc' : $("#id_chuyen_muc").val()
            }
            $.ajax({
                url : '/admin/san-pham/create',
                type : 'post',
                data : z,
                success : function(res){
                    if(res.status){
                        toastr.success('Thêm Mới Thành Công');
                        hienthi();
                    }
                }
            });
        });
        $('body').on('keyup','#ten_san_pham',function(){
            $("#slug_san_pham").val(toSlug($("#ten_san_pham").val()));
        });
        function toSlug(str) {
            // Chuyển chuỗi sang chữ thường
            str = str.toLowerCase();

            // Xóa dấu
            str = str.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
            str = str.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
            str = str.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
            str = str.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
            str = str.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
            str = str.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
            str = str.replace(/đ/gi, 'd');

            // Xóa khoảng trắng và ký tự đặc biệt
            str = str.replace(/\s+/g, '-'); // Thay khoảng trắng bằng dấu gạch ngang
            str = str.replace(/[^a-z0-9-]/gi, ''); // Xóa ký tự đặc biệt

            return str;
        }

    });
</script>
@endsection
