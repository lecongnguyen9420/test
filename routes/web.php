<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ChuyenMucController;
use App\Http\Controllers\SanPhamController;
use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;

Route::get('/jquerry', [TestController::class, 'jquerry']);
Route::get('/vue', [TestController::class, 'Vue']);

Route::get('/demo-data', [TestController::class, 'demoData']);

Route::get('/', function () {
    return view('welcome');
});


Route::get('/test', [TestController::class, 'inadex']);

Route::group(['prefix' => '/admin'], function() {
    // Route của Chuyên Mục
    Route::group(['prefix' => '/chuyen-muc'], function() {
        Route::get('/index', [ChuyenMucController::class, 'index']);
        Route::get('/data', [ChuyenMucController::class,'data']);
        Route::post('/create', [ChuyenMucController::class,'store']);

        Route::get('/doi-trang-thai/{id}',[ChuyenMucController::class, 'doiTrangThai']);
        Route::get('/delete/{id}',[ChuyenMucController::class, 'destroy']);
        Route::get('/edit/{id}',[ChuyenMucController::class, 'edit1']);
        Route::post('/update', [ChuyenMucController::class,'update']);
    });
    //route san-pham
    Route::group(['prefix'=>'/san-pham'],function(){
        
        Route::get('/index',[SanPhamController::class,'index']);

        Route::get('/index-old',[SanPhamController::class,'index_old']);
        Route::get('/data',[SanPhamController::class,'data']);
        Route::get('/get-data-id/{id}',[SanPhamController::class,'id_data']);
        Route::post('/update',[SanPhamController::class,'update']);
        Route::post('/create',[SanPhamController::class,'store']);
        Route::get('/delete/{id}',[SanPhamController::class,'xoa']);
    });
    //route tai-khoan
    Route::group(['prefix'=>'/tai-khoan'],function(){
        Route::get('/indexAD',[AdminController::class,'index']);
        // Route::get('/index-ajax',[AdminController::class,'index_ajax']);
        // Route::get('/index-vue',[AdminController::class,'index_vue']);


        Route::get('/data',[AdminController::class,'data']);
        // Route::post('/create', [AdminController::class,'store']);
        Route::post('/create-ajax', [AdminController::class,'create_ajax']);
    });


});


