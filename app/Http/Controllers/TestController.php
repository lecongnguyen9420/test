<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {
       return view('admin.share.master_page');
    }
    public function jquerry()
    {
       return view('admin.pages.demo.jquerry');
    }
    public function vue()
    {
       return view('admin.pages.demo.vue');
    }
    public function demoData()
    {
       return response()->json([
        'message' => 'Xin chào',
        'gia_tri'   => random_int(1, 100)
       ]);
    }
}
