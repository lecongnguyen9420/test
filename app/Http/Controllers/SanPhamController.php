<?php

namespace App\Http\Controllers;

use App\Models\SanPham;
use Illuminate\Http\Request;

class SanPhamController extends Controller
{
    public function index_old()
    {
        return view('admin.pages.admin.index');
    }
    public function index()
    {
        return view('admin.pages.san_pham.index_vue');
    }
    public function data()
    {
        $sanpham = SanPham::get();
        return response()->json([
            'data' => $sanpham
        ]);
    }
    public function id_data($id)
    {
        $sanpham = SanPham::where('id', $id)->first();
        return response()->json([
            'data' => $sanpham
        ]);
    }
    public function update(Request $request)
    {
        $sanpham = SanPham::where('id', $request->id)->first();
        if ($sanpham) {
            $sanpham->id = $request->id;
            $sanpham->ten_san_pham =  $request->ten_san_pham;
            $sanpham->slug_san_pham = $request->slug_san_pham;
            $sanpham->tinh_trang = $request->tinh_trang;
            $sanpham->mo_ta = $request->mo_ta;
            $sanpham->id_chuyen_muc = $request->id_chuyen_muc;
            $sanpham->save();
            return response()->json([
                'status' => true
            ]);
        }
    }
    public function store(Request $request)
    {
        SanPham::create([
            'ten_san_pham' => $request->ten_san_pham,
            'slug_san_pham' => $request->slug_san_pham,
            'hinh_anh' => $request->hinh_anh,
            'mo_ta' => $request->mo_ta,
            'id_chuyen_muc' => $request->id_chuyen_muc,
            'tinh_trang' => $request->tinh_trang,
        ]);
        return response()->json([
            'status' => true
        ]);
    }
    public function xoa($id)
    {
        $a = SanPham::where('id', $id)->first();
        if ($a) {
            $a->delete();
            return response()->json([
                'status' => true
            ]);
        }
    }
}
