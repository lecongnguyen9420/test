<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use ReturnTypeWillChange;

class AdminController extends Controller
{
    public function index()
    {
        $data = Admin::get();
        return view('admin.pages.admin.index', compact('data'));
    }

    // public function index_ajax()
    // {

    //     return view('admin.pages.admin.index');
    // }

    // public function index_vue(){
    //     return view('admin.pages.admin.index_vue');
    // }

    // public function store(Request $request)
    // {
    //     dd($request->all());
    //     $data = $request->all();
    //     Admin::create($data);
    //     return redirect('/admin/tai-khoan/indexAD');
    // }

    public function create_ajax(Request $request)
    {
        // dd($request->all());
        $data = $request->all();

        Admin::create($data);

        return response()->json([
            'status' => true
        ]);
    }

    public function data()
    {
        $data = Admin::get();

        return response()->json([
            'data' => $data,
        ]);
    }
}
