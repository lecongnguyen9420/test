<?php

namespace App\Http\Controllers;

use App\Models\ChuyenMuc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\returnSelf;

class ChuyenMucController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.chuyenmuc.index');
    }

    public function data()
    {
        $sql = "SELECT A.*, B.ten_chuyen_muc as ten_chuyen_muc_cha
                FROM chuyen_mucs A
                LEFT JOIN chuyen_mucs B on A.id_chuyen_muc_cha = B.id";
        $data = DB::select($sql);


        return response()->json([
            'data' => $data
        ]);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {

        // dd($request->all());
        ChuyenMuc::create([
            'ten_chuyen_muc'        =>  $request->ten_chuyen_muc,
            'slug_chuyen_muc'       =>  $request->slug_chuyen_muc,
            'tinh_trang'            =>  $request->tinh_trang,
            'id_chuyen_muc_cha'     =>  $request->id_chuyen_muc_cha,
        ]);
        return response()->json([
            'xxx' => true
        ]);
    }

    public function doiTrangThai($id)
    {

        $chuyenMuc = ChuyenMuc::where('id', $id)->first();
        if ($chuyenMuc) {
            $chuyenMuc->tinh_trang = !$chuyenMuc->tinh_trang;
            $chuyenMuc->save();
            return response()->json([
                'status' => 'True',
            ]);
        } else {
            return response()->json([
                'status' => 'False',
            ]);
        }
    }

    public function destroy($id){
        $chuyenMuc = ChuyenMuc::where('id', $id)->first();
        if ($chuyenMuc) {
            $chuyenMuc->delete();
            return response()->json([
                'status' => true
            ]);
        } else {
            return response()->json([
                'status' => false
            ]);
        }
    }

    public function edit1($id){
        $chuyenMuc = ChuyenMuc::where('id', $id)->first();
        // dd($chuyenMuc);
        if ($chuyenMuc) {
            return response()->json([
                'status' => true,
                'data'   => $chuyenMuc,
            ]);
        } else {
            return response()->json([
                'status' => false
            ]);
        }
    }
    public function update(Request $request){
        $chuyenMuc = ChuyenMuc::find($request->id);
        if($chuyenMuc){
            //nếu có thì cập nhật và return
            $chuyenMuc->ten_chuyen_muc        =  $request->ten_chuyen_muc;
            $chuyenMuc->slug_chuyen_muc       =  $request->slug_chuyen_muc;
            $chuyenMuc->tinh_trang            =  $request->tinh_trang;
            $chuyenMuc->id_chuyen_muc_cha     =  $request->id_chuyen_muc_cha;
            $chuyenMuc->save();
            return response()->json([
                'status' => true
            ]);
        } else {
            return response()->json([
                'status' => false
            ]);
        }

    }
}
